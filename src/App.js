import React from "react";
import './App.css';
import Numbox from './Numbox/Numbox';

class App extends React.Component {
  getNums = () => {
    const numsArr = [];
    for (let i = 0; i < 5; i++) {
      let number = Math.floor(Math.random() * 32) + 5;
      for (let i = 0; i < numsArr.length; i++) {
        if (number === numsArr[i]) {
          number = Math.floor(Math.random() * 32) + 5;
        }
      }
      numsArr.push(number);
    }
    numsArr.sort(function (a, b) { return a - b })
    return (numsArr);
  };
  state = {
    numbers: [0,0,0,0,0,]
  };
  NewNumbers = () => {
    let numbers = [...this.state.numbers];
    numbers = this.getNums();
    console.log('Old Numbers: ' + this.state.numbers.join(', ') + '\nNew Nums: ' + numbers.join(', '));
    this.setState({ numbers });
  }
  render() {
    return (
      <div className="App">
        <Numbox
          newnums={this.NewNumbers}
          num1={this.state.numbers[0]}
          num2={this.state.numbers[1]}
          num3={this.state.numbers[2]}
          num4={this.state.numbers[3]}
          num5={this.state.numbers[4]}
        />
      </div>
    );
  };
};

export default App;
